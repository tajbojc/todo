FROM ubuntu

#CMD ["apt", "update"]
#CMD ["apt", "install", "-y", "python3"]

RUN apt-get update && apt-get install -y python3 pip nfs-common

RUN pip install django

RUN mkdir /code

COPY . /code/blog/  

WORKDIR /code/blog/

RUN pip install django-crispy-forms

RUN pip install crispy-bootstrap4

RUN pip install Pillow

RUN pip install tzdata
RUN pip install whitenoise
RUN python3 manage.py collectstatic --no-input

RUN rm /code/blog/db.sqlite3
RUN mkdir /code/blog/db

#CMD [ "python3", "manage.py", "runserver", "0.0.0.0:8000"]
CMD mount -vvv -o nolock 139.162.136.103:/db/todo /code/blog/db; python3 manage.py runserver 0.0.0.0:8000
# odakle i gdje tj. odakle na sta /POM usr/Qxf2

